function [error_train, error_val] = ...
    learningCurveRandom(X, y, Xval, yval, lambda)

% you need these values later
m = size(X,1)     % the number of training examples
r = size(Xval,1)  % the number of validation examples

% You need to return these values correctly
error_train = zeros(m, 1);
error_val   = zeros(m, 1);

for i = 1:m
  % create two empty vectors for the Jtrain and Jcv values
  N = 50;
  Jtrain = zeros(N, 1);
  Jcv = zeros(N, 1);
  for j = 1:N
    % use 'm' to select 'i' random examples from the training set
    % use 'r' to select 'i' random examples from the validation set
    indices = randperm(m);
    X_ = X(indices(1:i),:);
    y_ = y(indices(1:i),:);

    indices = randperm(r);
    Xval_ = Xval(indices(1:i),:);
    yval_ = yval(indices(1:i),:);

    % compute theta
    % compute Jtrain and Jcv and save the values
    theta = trainLinearReg(X_, y_, lambda);
    Jtrain(j) = linearRegCostFunction(X_, y_, theta, 0);
    Jcv(j) = linearRegCostFunction(Xval_, yval_, theta, 0);
  end
  % compute the mean of the Jtrain vector and save it in error_train(i)
  % compute the mean of the Jcv vector and save it in error_val(i)
  error_train(i) = mean(Jtrain);
  error_val(i) = mean(Jcv);
end

% -------------------------------------------------------------

% =========================================================================

end
